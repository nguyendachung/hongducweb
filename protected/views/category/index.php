<div class="columns-container">
    <div class="container" id="columns">
        <!-- breadcrumb -->
        <div class="breadcrumb clearfix">
            <a class="home" href="#" title="Return to Home">Home</a>
            <span class="navigation-pipe">&nbsp;</span>
            <span class="navigation_page"><?php echo Yii::app()->getController()->getId() ?></span>
            <input type="hidden" value="<?php echo Yii::app()->request->getParam('txtId'); ?>" id="txtId"/>
            <input type="hidden" value="<?php echo Yii::app()->request->getParam('id'); ?>" id="id"/>
        </div>

        <!-- ./breadcrumb -->
        <!-- row -->
        <div class="row">
            <!-- Left colunm -->
            <div class="column col-xs-12 col-sm-3" id="left_column">

                <div class="menu-left">
                    <div class="box-vertical-megamenus">
                        <h4 class="title">
                            <span class="title-menu">Danh Mục Sản Phẩm</span>
                            <span class="btn-open-mobile pull-right "><i class="fa fa-bars"></i></span>
                        </h4>
                        <div class="vertical-menu-content is-home">
                            <ul class="vertical-menu-list">
                                <?php
                                //edit by:leTham. 8/10
                                //truyen du lieu len view thong qua ham o widget->leftMenu
                                foreach ($dataLeftMenu as $item) {
                                    ?>
                                    <li>
                                        <a class="parent" href="#">
                                            <img class="icon-menu" alt="Funky roots"
                                                 src="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo $item['image_icon'] ?>">
                                            <?php echo $item['name'] ?>
                                        </a>
                                        <div class="vertical-dropdown-menu">
                                            <div class="vertical-groups col-sm-12">
                                                <?php
                                                foreach ($item['subCat'] as $subitem) {
                                                    ?>
                                                    <div class="mega-group col-sm-4">
                                                        <h4 class="mega-group-header">
                                                            <a href="<?php echo Yii::app()->request->baseUrl . '/Category/ListProduct/id/' . $subitem['id_category']; ?>">
                                                                <?php echo $subitem['name'] ?>
                                                            </a></h4>
                                                        <ul class="group-link-default">
                                                            <?php
                                                            foreach ($subitem['Cat'] as $cat) {
                                                                ?>
                                                                <li>
                                                                    <a href="<?php echo Yii::app()->request->baseUrl . '/Category/ListProduct/id/' . $cat['id_category']; ?>">
                                                                        <?php echo $cat['name'] ?>
                                                                    </a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ./left colunm -->
            <!-- Center colunm-->
            <div class="center_column col-xs-12 col-sm-9" id="center_column">
                <!-- category-slider -->
                <div class="category-slider">
                    <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav="true"
                        data-autoplayTimeout="1000" data-autoplayHoverPause="true" data-items="1">
                        <li>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/data/category-slide.jpg"
                                 alt="category-slider">
                        </li>
                        <li>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/data/slide-cart2.jpg"
                                 alt="category-slider">
                        </li>
                    </ul>
                </div>
                <!-- ./category-slider -->
                <!-- subcategories -->

                <!-- ./subcategories -->
                <!-- view-product-list-->
                <div id="view-product-list" class="view-product-list">
                    <h2 class="page-heading">
                        <span class="page-heading-title"><?php echo $cat->name ?></span>
                    </h2>
                    <ul class="display-product-option">
                        <li class="view-as-grid selected">
                            <span>grid</span>
                        </li>
                        <li class="view-as-list">
                            <span>list</span>
                        </li>
                    </ul>
                    <!-- PRODUCT LIST -->
                    <ul class="row product-list grid">
                        <?php
                        if ($data) {
                            foreach ($data as $item) {

                                ?>
                                <li class="col-sx-12 col-sm-4">
                                    <div class="product-container">
                                        <div class="left-block">
                                            <a href="<?php echo Yii::app()->request->baseUrl . '/Detail/Detail/id/'; ?><?php echo $item['id_product'] ?>">
                                                <img id="img_<?php echo $item['id_product'] ?>" class="img-responsive"
                                                     alt="product"
                                                     src="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo $item['image'] ?>"/>
                                            </a>
                                            <!-- <div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                                    <a title="Add to compare" class="compare" href="#"></a>
                                                    <a title="Quick view" class="search" href="#"></a>
                                            </div> -->
                                            <div class="add-to-cart">
                                                <button class="" type="submit"
                                                        onclick="showAddCart('<?php echo $item['id_product'] ?>')">Xem
                                                    Nhanh
                                                </button>
                                            </div>
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a id="name_<?php echo $item['id_product'] ?>"
                                                                        href="<?php echo Yii::app()->request->baseUrl . '/Detail/Detail/id/'; ?><?php echo $item['id_product'] ?>"><?php echo $item['name'] ?></a>
                                            </h5>
                                            <div class="product-star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </div>
                                            <div class="content_price">
                                                <span id="pri_<?php echo $item['id_product'] ?>"
                                                      class="price product-price">Liên Hệ</span>
                                                <span class="description hidden"><?php echo $item['description'] ?></span>
                                            </div>
                                            <div class="info-orther">
                                                <p>Item Code: <?php echo $item['id_product'] ?></p>
                                                <p class="availability">Availability:
                                                    <span><?php echo $item['quanty'] ?></span></p>
                                                <div class="product-desc">
                                                    <?php echo $item['description'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        } else {
                            echo 'Không có dữ liệu !';
                        }
                        ?>

                    </ul>
                    <!-- ./PRODUCT LIST -->
                </div>
                <!-- ./view-product-list-->
                <div class="sortPagiBar">

                    <div class="bottom-pagination">
                        <nav>
                            <?php
                            $this->widget('CLinkPager', array(
                                'currentPage' => $pages->getCurrentPage(),
                                'itemCount' => $item_count,
                                'pageSize' => $page_size,
                                'maxButtonCount' => 5,
                                'header' => '',
                                'firstPageLabel' => '|<',
                                'prevPageLabel' => '<<',
                                'nextPageLabel' => '>>',
                                'lastPageLabel' => '>|',
                                'htmlOptions' => array(
                                    'class' => 'pagination'
                                ),
                            ));
                            ?>

                        </nav>
                    </div>

                </div>
            </div>
            <!-- ./ Center colunm -->
        </div>
        <!-- ./row-->
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<!--<script language="Javascript" type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
<script type="text/javascript">

    $(document).ready(function () {

        $("#check-box-list input[type=checkbox]").click(function () {
            var myClass = $(this).attr("class");

            getValueUsingClass(myClass);
        });
    });

    function getValueUsingClass(myClass) {
        var chkArray = [];
        $("." + myClass + ":checked").each(function () {
            chkArray.push($(this).val());
        });
        var selected;
        selected = chkArray.join(',');
        if (selected.length >= 1) {
            var temm = $('#txtId').val();
            var id = $('#id').val();
            if (temm.length > 0) {
                //   selected=temm+","+selected;
                //    selected=temm;
            }
            window.location.href = "<?php echo Yii::app()->request->baseUrl; ?>/Category/ListProduct/id/" + id + "/type/" + myClass + "/txtId/" + selected + "";

        } else {
            alert("Please at least one of the checkbox");
        }
    }

    function getSpecial(myClass) {
        var id = $('#id').val();
        window.location.href = "<?php echo Yii::app()->request->baseUrl; ?>/Category/ListProduct/id/" + id + "/type/" + myClass + "";
    }

</script>
