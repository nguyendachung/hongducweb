<?php

class DetailController extends Controller
{
        public function init() {
            parent::init();
             $this->layout='//layouts/category';
        }
	public function actionIndex()
	{
		   header('Content-type: application/json');
                     echo CJSON::encode("Err");
                    Yii::app()->end();
	}
        public function  actionDetail($id){
             if ($id != null) {
                   $data=  Product::getProductbyID($id);
                   foreach ($data as $item){
                       $name=$item['name'];
                   }
                    // lấy sản phẩm tương tự
                  $similar=  Product::getProductSimilar($name);
                   // lấy sản phẩm Nội thất Hồng Đức đề cử
                  $better=  Product::getProductBetter(30);
                  // lấy sản phẩm bán chạy
                  $betterLeft=  Product::getProductBetter(3);
                   // lấy quảng cáo
                  $ads=  Ads::getAdsLimit(3);
                  // giảm giá
                  $sale=  Product::getProductSale(3);
                 // gọi đến color
                $color= Color::getAllColor('id_color,name');
                $temColor=array();
                foreach($color as $item){
                    $temColor[$item['id_color']]=$item['name'];
                }
                // gọi đến size
                $size=  Size::getAllSize('id_size,name');
                $temSize=array();
                foreach($size as $item){
                    $temSize[$item['id_size']]=$item['name'];
                }
                   $this->render('index',array (
                       'data'=>$data,
                        'Color'=>$temColor,
                        'Size'=>$temSize,
                        'similar'=>$similar,
                        'better'=>$better,
                        'betterLeft'=>$betterLeft,
                        'ads'=>$ads,
                        'sale'=>$sale,
                         'dataLeftMenu' => $this->menuLeftCata()
                        ));
             }else{
                  header('Content-type: application/json');
                     echo CJSON::encode("Err");
                    Yii::app()->end();
             }
        }

    public  function menuLeftCata(){
        $parent = Category::getAllParent();//ham lay menu cap 1
        $cat = new Category;
        $dataCat = $cat->getAllCategory('id_category,name');
        $temDataCat = array();
        foreach ($dataCat as $item) {
            $temDataCat[$item['id_category']] = $item['name'];
        }
        foreach ($parent as &$item) {// lap de lay menu cap 2
            $item['subCat'] = Category::getAllCategoryBy($item['id_category']);
            foreach ($item['subCat'] as &$subItem) {// lay menu cap 3
                $subItem['Cat'] = Category::getAllCategoryBy($subItem['id_category']);
            }
        }
        return $parent;

//        $this->render("CategoryleftMenu", array('data' => $parent, 'catagory' => $temDataCat));
    }
}