<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo Yii::app()->request->baseUrl; ?>/"><img class="logo" alt="Nội Thất Hồng Đức"
                                                                        src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php
                $name_controller = Yii::app()->getController()->getId();
                $class_active = "";
                if ($name_controller == "home") { ?>
                    <li class="active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/">Trang chủ</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/">Trang chủ</a></li>
                <?php }
                if ($name_controller == "news") { ?>
                    <li class="active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/News">Tin Tức</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/News">Tin Tức</a></li>
                <?php } ?>


                <?php foreach ($data as $item) { ?>
                    <li class="dropdown">
                        <a href="<?php echo Yii::app()->request->baseUrl . '/Category/ListProduct/id/' . $item['id_category'] ?>"
                           class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            <?php echo $item['name'] ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach ($item['subCat'] as $subitem) { ?>
                                <li>
                                    <a href="<?php echo Yii::app()->request->baseUrl . '/Category/ListProduct/id/' . $subitem['id_category']; ?> ">
                                        <?php echo $subitem['name'] ?>
                                    </a>
                                </li>
                                <?php foreach ($subitem['Cat'] as $cat) { ?>
                                    <!-- <li>
                                        <a href="<?php echo Yii::app()->request->baseUrl . '/Category/ListProduct/id/' . $cat['id_category']; ?> ">
                                            <?php echo $cat['name'] ?>
                                        </a>
                                    </li> -->
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php
                $name_controller = Yii::app()->getController()->getId();
                $class_active = "";
                if ($name_controller == "contact") {
                    ?>
                    <li class="active"><a href="<?php echo Yii::app()->request->baseUrl; ?>/Contact">Liên Hệ</a></li>
                <?php } else {
                    ?>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/Contact">Liên Hệ</a></li>
                <?php }
                ?>
                <li class="header-search-box">
                    <div class="form-inline">
                        <div class="form-group input-serach">
                            <input type="text" id="seach_key" placeholder="Keyword here..."/>
                            <button class="pull-right btn-search" onclick="seach()"><span class="fa fa-search"></span>
                            </button>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function seach() {
        $caID = $('#select_seach').val();
        $key = $('#seach_key').val();
        if ($key.length > 0) {
            window.location.href = "<?php echo Yii::app()->request->baseUrl; ?>/Seach/index/CatID//Key/" + $key + "";
        }
    }
</script>



